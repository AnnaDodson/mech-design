Attribution Requirements

As an open source project, attribution is critical from a legal, practical and motivational perspective. Please give us credits! Common places for attribution are for example: to mention us in your project README, the 'About' section or the footer on a website/in mobile apps.

Attribution example:

    All emojis designed by OpenMoji – the open-source emoji and icon project. License: CC BY-SA 4.0

    License

    The emojis are licensed under the Creative Commons Share Alike 4.0 (CC BY-SA 4.0) license.
